# Pet

## Domain

This small, desktop app roughly imitates interaction with a being called *Pet*

*Pet* has *Needs* which influences *Satisfaction*. *Pet* likes *Hugs*.
More *Hugs*, better *Satisfaction*, less, then *Satisfaction* decreases.

## Technology

This project experiments with Kotlin Coroutines `actors` when it comes to share mutable state management.

### Needs

#### Hug

*Hug* is a need to hug *Pet*. If *Hug* is not received  since some time, it will decrease *Satisfaction*.
If *Hug* is received, it will increase *Satisfaction*

## Interactions

Interaction is made by command line console. Also, in the console, there will be displayed all *Events* which happened on *Pet*.
*Hug* need is monitored so it will be shown whenever *Hug* is not given since some time.

### Messages

There is console based interactions. Allow messages are:

- h - give a normal hug
- bh - give a big hug


### Events

*Pet* as a reaction on *Message* can emit 0 or multiple events from below list:

- SatisfactionIncreased - reaction on given *Hug*
- SatisfactionDecreased - when there is no *Hug* given since some time
- HugGiven - when *Hug* is given


## State

The state of *Pet* is build upon all published *Events* -> event sourcing.
Currently, it's held only in memory so won't remain between lunches -> shame on me

## Build and run

**Requirements**:

1. Gradle
1. JVM

Project is made with Kotlin and requires JVM to run executable JAR.
To build it, use build commands form *gradle kotlin dsl* building tool.

- invoke ```gradle build``` to build it
- invoke ```java build/libs/pet.jar``` to run it

## TODO

1. More needs and interactions
1. Make it configurable
1. Policies for thresholds
1. Add persistency
1. Add proper logging