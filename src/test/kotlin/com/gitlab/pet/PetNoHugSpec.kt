package com.gitlab.pet

import com.gitlab.pet.domain.HugGiven
import com.gitlab.pet.domain.PetEvent
import com.gitlab.pet.domain.PetFactory.create
import com.gitlab.pet.domain.Satisfaction.Low
import com.gitlab.pet.domain.Satisfaction.Normal
import com.gitlab.pet.domain.SatisfactionDecreased
import com.gitlab.pet.domain.needs.Hug.NormalHug
import io.kotlintest.matchers.beEmpty
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.should
import io.kotlintest.specs.BehaviorSpec
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.Month

class PetNoHugSpec : BehaviorSpec({

    Given("Pet with recent hug") {
        val pet = create(dateOfBirth, hugGiven(now()))
        When("notify about no hug") {
            val events = pet.notifyNoHug()
            Then("no events published") {
                events should beEmpty()
            }
        }
    }

    Given("Pet with no hug since short while") {
        val pet = create(dateOfBirth, hugGiven(now().minusMinutes(1)))
        When("notify about no hug") {
            val events = pet.notifyNoHug()
            Then("satisfaction is decreased slightly") {
                events shouldContainExactly setOf(SatisfactionDecreased(Normal(4)))
            }
        }
    }

    Given("Pet with no hug since long while") {
        val pet = create(dateOfBirth, hugGiven(now().minusMinutes(2)))
        When("notify about no hug") {
            val events = pet.notifyNoHug()
            Then("satisfaction is decreased slightly") {
                events shouldContainExactly setOf(SatisfactionDecreased(Low(-5)))
            }
        }
    }

})

private val dateOfBirth = LocalDateTime.of(1990, Month.FEBRUARY, 6, 10, 45)
private fun hugGiven(hugDate: LocalDateTime) = listOf<PetEvent>(HugGiven(NormalHug(hugDate)))