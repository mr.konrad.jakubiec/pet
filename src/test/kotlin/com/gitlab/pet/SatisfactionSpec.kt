package com.gitlab.pet

import com.gitlab.pet.domain.Satisfaction.*
import com.gitlab.pet.domain.Satisfaction.Unit.HUGELY
import com.gitlab.pet.domain.Satisfaction.Unit.SLIGHTLY
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row

class SatisfactionSpec : StringSpec({

    "should throw exception when satisfaction level is out of range" {
        forall(
            row({ Normal(-1) }),
            row({ Normal(10) }),
            row({ High(9) }),
            row({ High(100) }),
            row({ VeryHigh(99) }),
            row({ VeryHigh(Int.MAX_VALUE) }),
            row({ Low(-100) }),
            row({ Low(0) }),
            row({ VeryLow(-99) }),
            row({ VeryLow(Int.MIN_VALUE) })
        ) { satisfaction ->
            shouldThrow<SatisfactionLevelOutOfRange> { satisfaction() }

        }
    }

    "should increase satisfaction" {
        forall(
            row(SLIGHTLY, VeryLow(-110), VeryLow(-109)),
            row(HUGELY, VeryLow(-110), VeryLow(-100)),
            row(SLIGHTLY, VeryLow(-100), Low(-99)),
            row(HUGELY, VeryLow(-100), Low(-90)),
            row(SLIGHTLY, Low(-99), Low(-98)),
            row(HUGELY, Low(-99), Low(-89)),
            row(SLIGHTLY, Low(-1), Normal(0)),
            row(HUGELY, Low(-10), Normal(0)),
            row(SLIGHTLY, Normal(0), Normal(1)),
            row(HUGELY, Normal(0), High(10)),
            row(SLIGHTLY, High(10), High(11)),
            row(HUGELY, High(10), High(20)),
            row(SLIGHTLY, High(99), VeryHigh(100)),
            row(HUGELY, High(99), VeryHigh(109)),
            row(SLIGHTLY, VeryHigh(100), VeryHigh(101)),
            row(HUGELY, VeryHigh(100), VeryHigh(110))
        ) { unit, current, expected ->
            current.increase(unit) shouldBe expected
        }
    }

    "should decrease satisfaction" {
        forall(
            row(SLIGHTLY, VeryLow(-110), VeryLow(-111)),
            row(HUGELY, VeryLow(-110), VeryLow(-120)),
            row(SLIGHTLY, Low(-99), VeryLow(-100)),
            row(HUGELY, Low(-90), VeryLow(-100)),
            row(SLIGHTLY, Low(-90), Low(-91)),
            row(HUGELY, Low(-80), Low(-90)),
            row(SLIGHTLY, Normal(0), Low(-1)),
            row(HUGELY, Normal(0), Low(-10)),
            row(SLIGHTLY, Normal(1), Normal(0)),
            row(HUGELY, High(10), Normal(0)),
            row(SLIGHTLY, High(11), High(10)),
            row(HUGELY, High(20), High(10)),
            row(SLIGHTLY, VeryHigh(100), High(99)),
            row(HUGELY, VeryHigh(109), High(99)),
            row(SLIGHTLY, VeryHigh(101), VeryHigh(100)),
            row(HUGELY, VeryHigh(110), VeryHigh(100))
        ) { unit, current, expected ->
            current.decrease(unit) shouldBe expected
        }
    }

})