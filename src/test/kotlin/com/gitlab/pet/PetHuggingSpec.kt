package com.gitlab.pet

import com.gitlab.pet.domain.HugGiven
import com.gitlab.pet.domain.PetFactory
import com.gitlab.pet.domain.Satisfaction.High
import com.gitlab.pet.domain.Satisfaction.Normal
import com.gitlab.pet.domain.SatisfactionIncreased
import com.gitlab.pet.domain.needs.Hug
import com.gitlab.pet.domain.needs.Hug.BigHug
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.specs.BehaviorSpec
import java.time.LocalDateTime
import java.time.Month

class PetHuggingSpec : BehaviorSpec({

    Given("Pet with normal satisfaction") {
        val pet = PetFactory.create(LocalDateTime.of(1990, Month.FEBRUARY, 6, 10, 45))
        When("normal hug is received") {
            val normalHug = Hug.NormalHug(LocalDateTime.now())
            val events = pet.hug(normalHug)
            Then("satisfaction is increased and hug is given") {
                events shouldContainExactly setOf(SatisfactionIncreased(Normal(6)), HugGiven(normalHug))
            }
        }
        When("big hug is received") {
            val bigHug = BigHug(LocalDateTime.now())
            val events = pet.hug(bigHug)
            Then("satisfaction is increased and hug is given") {
                events shouldContainExactly setOf(SatisfactionIncreased(High(15)), HugGiven(bigHug))
            }
        }
    }
})