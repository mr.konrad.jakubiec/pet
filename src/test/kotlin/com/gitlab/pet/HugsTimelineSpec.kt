package com.gitlab.pet

import com.gitlab.pet.domain.HugsTimeline
import io.kotlintest.properties.Gen
import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit
import java.util.concurrent.ThreadLocalRandom


class HugsTimelineSpec : StringSpec({

    "last hug since property" {
        forAll(
            timeGenerator,
            timeGenerator
        ) { lastHug: LocalDateTime, since: LocalDateTime ->
            HugsTimeline(lastHug).lastHugSince(since) == lastHug.until(since, ChronoUnit.SECONDS)
        }
    }

})

private val minDay = LocalDateTime.of(1970, 1, 1, 0, 0, 0).toEpochSecond(ZoneOffset.UTC)
private val maxDay = LocalDateTime.of(2200, 12, 31, 0, 0, 0).toEpochSecond(ZoneOffset.UTC)
private fun nextRandomLong() = ThreadLocalRandom.current().nextLong(
    minDay,
    maxDay
)

private val timeGenerator = object : Gen<LocalDateTime> {

    override fun constants(): Iterable<LocalDateTime> = emptyList()

    override fun random(): Sequence<LocalDateTime> = generateSequence {
        LocalDateTime.ofInstant(Instant.ofEpochSecond(nextRandomLong()), ZoneId.systemDefault())
    }

}

