package com.gitlab.pet.adapters

import com.gitlab.pet.application.GiveHug
import com.gitlab.pet.application.Message
import com.gitlab.pet.domain.needs.Hug.BigHug
import com.gitlab.pet.domain.needs.Hug.NormalHug

import java.time.LocalDateTime.now

object ConsoleMessageReader {

    fun read(): Message? =
        translateToMessage(readLine())

    private fun translateToMessage(line: String?): Message? = when (line?.toUpperCase()) {
        "H" -> GiveHug(NormalHug(now()))
        "BH" -> GiveHug(BigHug(now()))
        else -> unknownMessage(line)
    }

    private fun unknownMessage(line: String?): Message? {
        println("[ConsoleMessageReader]: Unknown message [$line]")
        return null
    }
}
