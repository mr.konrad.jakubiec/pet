package com.gitlab.pet.application

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

typealias ReadMessage = () -> Message?

fun CoroutineScope.interact(brain: Brain, readMessage: ReadMessage) = launch(Dispatchers.IO) {

    while (isActive) {
        readMessage()?.let { brain.send(it) }
    }
}