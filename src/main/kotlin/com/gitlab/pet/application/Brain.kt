package com.gitlab.pet.application

import com.gitlab.pet.domain.PetEvent
import com.gitlab.pet.domain.PetFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import java.time.LocalDateTime

typealias Brain = SendChannel<Message>

fun CoroutineScope.brain() = actor<Message> {
    var pet = PetFactory.create(LocalDateTime.now())

    fun reactOn(message: Message): Set<PetEvent> = when (message) {
        is NoHugGiven -> pet.notifyNoHug()
        is GiveHug -> pet.hug(message.hug)
    }

    consumeEach { message ->
        println("[Brain] Received message [$message]")
        reactOn(message).let { events ->
            println("[Pet] reacted with [$events]")
            pet = PetFactory.apply(pet, events.toList())
        }
    }

}

