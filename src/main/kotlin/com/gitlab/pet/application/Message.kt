package com.gitlab.pet.application

import com.gitlab.pet.domain.needs.Hug

sealed class Message

object NoHugGiven : Message() {
    override fun toString(): String = this.javaClass.simpleName
}

data class GiveHug(val hug: Hug) : Message()