package com.gitlab.pet.application

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

fun CoroutineScope.monitorNeeds(brain: Brain) = launch {

    ticker(delayMillis = 5000, initialDelayMillis = 3000)
        .consumeEach { brain.send(NoHugGiven) }
}