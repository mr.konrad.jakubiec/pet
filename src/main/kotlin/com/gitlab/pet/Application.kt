package com.gitlab.pet

import com.gitlab.pet.adapters.ConsoleMessageReader
import com.gitlab.pet.application.brain
import com.gitlab.pet.application.interact
import com.gitlab.pet.application.monitorNeeds
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    launch {
        val brain = brain()
        monitorNeeds(brain)
        interact(brain, ConsoleMessageReader::read)
    }
    println("Pet woke up and need a hug!")
}