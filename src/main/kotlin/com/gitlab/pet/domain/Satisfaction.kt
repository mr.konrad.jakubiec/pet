package com.gitlab.pet.domain

sealed class Satisfaction(
    private val level: Int,
    private val range: ClosedRange<Int> = IntRange(Int.MIN_VALUE, Int.MAX_VALUE)

) {

    abstract fun same(level: Int): Satisfaction

    init {
        if (!range.contains(level)) throw SatisfactionLevelOutOfRange(level, range)
    }

    fun increase(unit: Unit): Satisfaction = create(level + unit)

    fun decrease(unit: Unit): Satisfaction = create(level - unit)

    enum class Unit(val value: Int) {
        SLIGHTLY(1), HUGELY(10)
    }

    private operator fun Int.plus(unit: Unit) = this + unit.value

    private operator fun Int.minus(unit: Unit) = this - unit.value

    private fun create(level: Int): Satisfaction = when {
        range.contains(level) -> same(level)
        level < range.start -> previous(level)
        level > range.endInclusive -> next(level)
        else -> throw SatisfactionLevelOutOfRange(level, range)
    }

    private fun next(level: Int) = when (this) {
        is VeryLow -> Low(level)
        is Low -> Normal(level)
        is Normal -> High(level)
        is High -> VeryHigh(level)
        is VeryHigh -> throw SatisfactionLevelOutOfRange(level, range)
    }

    private fun previous(level: Int) = when (this) {
        is VeryLow -> throw SatisfactionLevelOutOfRange(level, range)
        is Low -> VeryLow(level)
        is Normal -> Low(level)
        is High -> Normal(level)
        is VeryHigh -> High(level)
    }

    data class Normal(private val level: Int) : Satisfaction(level, 0..9) {
        override fun same(level: Int): Satisfaction = copy(level = level)
    }

    data class High(private val level: Int) : Satisfaction(level, 10..99) {
        override fun same(level: Int): Satisfaction = copy(level = level)
    }

    data class VeryHigh(private val level: Int) : Satisfaction(level, 100 until Int.MAX_VALUE) {
        override fun same(level: Int): Satisfaction = copy(level = level)
    }

    data class Low(private val level: Int) : Satisfaction(level, -99..-1) {
        override fun same(level: Int): Satisfaction = copy(level = level)
    }

    data class VeryLow(private val level: Int) : Satisfaction(level, (Int.MIN_VALUE + 1)..-100) {
        override fun same(level: Int): Satisfaction = copy(level = level)
    }

    class SatisfactionLevelOutOfRange(level: Int, range: ClosedRange<Int>) :
        RuntimeException("Satisfaction level [$level] out of range [$range]")
}