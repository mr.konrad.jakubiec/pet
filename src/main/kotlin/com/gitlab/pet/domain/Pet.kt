package com.gitlab.pet.domain

import com.gitlab.pet.domain.Satisfaction.Unit.HUGELY
import com.gitlab.pet.domain.Satisfaction.Unit.SLIGHTLY
import com.gitlab.pet.domain.needs.Hug
import com.gitlab.pet.domain.needs.Hug.BigHug
import com.gitlab.pet.domain.needs.Hug.NormalHug
import java.time.LocalDateTime

data class Pet internal constructor(private val satisfaction: Satisfaction, private val hugsTimeline: HugsTimeline) {

    private val slightNoHugRange = 60..119
    private val hugeNoHugRange = 120..Long.MAX_VALUE

    fun hug(hug: Hug): Set<PetEvent> = when (hug) {
        is NormalHug -> setOf(SatisfactionIncreased(satisfaction.increase(SLIGHTLY)), HugGiven(hug))
        is BigHug -> setOf(SatisfactionIncreased(satisfaction.increase(HUGELY)), HugGiven(hug))
    }

    fun notifyNoHug(): Set<PetEvent> = when (hugsTimeline.lastHugSince(LocalDateTime.now())) {
        in slightNoHugRange -> setOf(SatisfactionDecreased(satisfaction.decrease(SLIGHTLY)))
        in hugeNoHugRange -> setOf(SatisfactionDecreased(satisfaction.decrease(HUGELY)))
        else -> emptySet()
    }
}