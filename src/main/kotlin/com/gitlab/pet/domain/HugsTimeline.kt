package com.gitlab.pet.domain

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

internal data class HugsTimeline(private val lastHug: LocalDateTime) {

    fun lastHugSince(since: LocalDateTime): Long = lastHug.until(since, ChronoUnit.SECONDS)

}