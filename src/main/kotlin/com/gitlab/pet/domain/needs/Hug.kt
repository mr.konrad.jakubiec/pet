package com.gitlab.pet.domain.needs

import java.time.LocalDateTime

sealed class Hug {
    abstract val hugDate: LocalDateTime

    data class NormalHug(override val hugDate: LocalDateTime) : Hug()
    data class BigHug(override val hugDate: LocalDateTime) : Hug()
}