package com.gitlab.pet.domain

import com.gitlab.pet.domain.Satisfaction.Normal
import java.time.LocalDateTime

object PetFactory {

    fun create(dateOfBirth: LocalDateTime, events: List<PetEvent> = emptyList()): Pet =
        events
            .fold(initial(dateOfBirth), this::applyEvent)

    fun apply(pet: Pet, events: List<PetEvent>): Pet =
        events
            .fold(pet, this::applyEvent)

    private fun initial(dateOfBirth: LocalDateTime) = Pet(Normal(5), HugsTimeline((dateOfBirth)))

    private fun applyEvent(pet: Pet, event: PetEvent) = when (event) {
        is SatisfactionIncreased -> pet.copy(satisfaction = event.increasedSatisfaction)
        is HugGiven -> pet.copy(hugsTimeline = HugsTimeline(event.hug.hugDate))
        is SatisfactionDecreased -> pet.copy(satisfaction = event.decreasedSatisfaction)
    }
}