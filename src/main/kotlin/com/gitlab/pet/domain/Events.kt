package com.gitlab.pet.domain

import com.gitlab.pet.domain.needs.Hug

sealed class PetEvent

data class SatisfactionIncreased(val increasedSatisfaction: Satisfaction) : PetEvent()
data class SatisfactionDecreased(val decreasedSatisfaction: Satisfaction) : PetEvent()
data class HugGiven(val hug: Hug) : PetEvent()